..
 # Copyright (c) 2023, Linaro Ltd.
 #
 # SPDX-License-Identifier: MIT

############
Yocto Layers
############
TBD

.. todo::
   This should illustrate the layers which are integrated by the TRS project. We
   need a high level description of the layers included and why we include them.
   We also need a diagram like this:
   https://ewaol.docs.arm.com/en/kirkstone-dev/manual/yocto_layers.html#layer-dependency-overview

TRS recipes
***********
TRS leverage various layers and recipes to build firmware, root filesystem and
various images. The best place to start looking for those recipes is in the
default_ manifest file located in trs-manifest.git_ repository.

.. _default: https://gitlab.com/Linaro/trusted-reference-stack/trs-manifest/-/blob/main/default.xml
.. _trs-manifest.git: https://gitlab.com/Linaro/trusted-reference-stack/trs-manifest
