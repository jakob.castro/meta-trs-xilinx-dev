#!/bin/bash

# Copyright (c) 2023, Linaro Limited.
#
# SPDX-License-Identifier: MIT

set -e

declare -A PROPERTIES=(
    ["FirmwareTimestampMonotonic"]="us"
    ["LoaderTimestampMonotonic"]="us"
    ["KernelTimestampMonotonic"]="us"
    ["InitRDTimestampMonotonic"]="us"
    ["UserspaceTimestampMonotonic"]="us"
    ["FinishTimestampMonotonic"]="us"
)
function read_property {
    dbus-send --system --print-reply=literal \
        --dest=org.freedesktop.systemd1 \
        /org/freedesktop/systemd1 \
        org.freedesktop.DBus.Properties.Get \
        string:"org.freedesktop.systemd1.Manager" string:"$1" | awk '{print $3}'
}

# wait for boot to complete
systemctl is-system-running --wait

for property in "${!PROPERTIES[@]}"
do
    val=$(read_property "$property")
    unit=${PROPERTIES[$property]}
    lava-test-case "systemd-$property" \
        --result pass \
        --measurement "$val" \
        --units "$unit"
done
