# Copyright (c) 2023, Linaro Limited.
#
# SPDX-License-Identifier: MIT

SUMMARY = "LAVA test case for systemd metrics"
DESCRIPTION = "Support scripts to report systemd metrics such as boot time"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI = "file://lava-report-systemd-metrics.sh"

inherit allarch

do_install() {
	install -Dm0755 ${WORKDIR}/lava-report-systemd-metrics.sh ${D}${bindir}/lava-report-systemd-metrics.sh
}

RDEPENDS:${PN} += "bash"
RDEPENDS:${PN} += "dbus-tools"

