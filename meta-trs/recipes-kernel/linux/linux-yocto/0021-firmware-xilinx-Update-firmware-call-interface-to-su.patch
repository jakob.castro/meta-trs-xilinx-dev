Upstream-Status: Pending
From d2451834fd7cd3bccf850dfb6eca27f8339d6338 Mon Sep 17 00:00:00 2001
From: Izhar Ameer Shaikh <izhar.ameer.shaikh@xilinx.com>
Date: Thu, 20 Oct 2022 09:33:32 +0200
Subject: [PATCH 021/295] firmware: xilinx: Update firmware call interface to
 support additional arg

System-level platform management layer (do_fw_call()) has support for
maximum of 5 arguments as of now (1 EEMI API ID + 4 command arguments).
In order to support new EEMI PM_IOCTL IDs (Secure Read/Write), this
support must be extended to support one additional argument, which
results in a configuration of - 1 EEMI API ID + 5 command arguments.

Update zynqmp_pm_invoke_fn() and do_fw_call() with this new definition
containing additional argument. As a result, update all the references
to pm invoke function with the updated definition.

Signed-off-by: Izhar Ameer Shaikh <izhar.ameer.shaikh@xilinx.com>
Reviewed-by: Tanmay Shah <tanmay.shah@xilinx.com>
State: pending
---
 drivers/firmware/xilinx/zynqmp-debug.c  |   7 +-
 drivers/firmware/xilinx/zynqmp.c        | 205 +++++++++++++-----------
 drivers/soc/xilinx/xlnx_event_manager.c |   2 +-
 drivers/soc/xilinx/zynqmp_power.c       |   2 +-
 include/linux/firmware/xlnx-zynqmp.h    |   2 +-
 5 files changed, 117 insertions(+), 101 deletions(-)

diff --git a/drivers/firmware/xilinx/zynqmp-debug.c b/drivers/firmware/xilinx/zynqmp-debug.c
index 9f33dbf2f7aa..ea11e1771e9e 100644
--- a/drivers/firmware/xilinx/zynqmp-debug.c
+++ b/drivers/firmware/xilinx/zynqmp-debug.c
@@ -83,7 +83,7 @@ static int zynqmp_pm_self_suspend(const u32 node, const u32 latency,
 				  const u32 state)
 {
 	return zynqmp_pm_invoke_fn(PM_SELF_SUSPEND, node, latency,
-				   state, 0, NULL);
+				   state, 0, 0, NULL);
 }
 
 /**
@@ -95,7 +95,8 @@ static int zynqmp_pm_self_suspend(const u32 node, const u32 latency,
  */
 static int zynqmp_pm_abort_suspend(const enum zynqmp_pm_abort_reason reason)
 {
-	return zynqmp_pm_invoke_fn(PM_ABORT_SUSPEND, reason, 0, 0, 0, NULL);
+	return zynqmp_pm_invoke_fn(PM_ABORT_SUSPEND, reason, 0, 0, 0, 0,
+				   NULL);
 }
 
 /**
@@ -111,7 +112,7 @@ static int zynqmp_pm_abort_suspend(const enum zynqmp_pm_abort_reason reason)
 static int zynqmp_pm_ioctl(const u32 node, const u32 ioctl, const u32 arg1,
 			   const u32 arg2, u32 *out)
 {
-	return zynqmp_pm_invoke_fn(PM_IOCTL, node, ioctl, arg1, arg2, out);
+	return zynqmp_pm_invoke_fn(PM_IOCTL, node, ioctl, arg1, arg2, 0, out);
 }
 
 /**
diff --git a/drivers/firmware/xilinx/zynqmp.c b/drivers/firmware/xilinx/zynqmp.c
index d304c11950fd..8e04ee223317 100644
--- a/drivers/firmware/xilinx/zynqmp.c
+++ b/drivers/firmware/xilinx/zynqmp.c
@@ -113,7 +113,7 @@ static int zynqmp_pm_ret_code(u32 ret_status)
 	}
 }
 
-static noinline int do_fw_call_fail(u64 arg0, u64 arg1, u64 arg2,
+static noinline int do_fw_call_fail(u64 arg0, u64 arg1, u64 arg2, u64 arg3,
 				    u32 *ret_payload)
 {
 	return -ENODEV;
@@ -123,25 +123,26 @@ static noinline int do_fw_call_fail(u64 arg0, u64 arg1, u64 arg2,
  * PM function call wrapper
  * Invoke do_fw_call_smc or do_fw_call_hvc, depending on the configuration
  */
-static int (*do_fw_call)(u64, u64, u64, u32 *ret_payload) = do_fw_call_fail;
+static int (*do_fw_call)(u64, u64, u64, u64, u32 *ret_payload) = do_fw_call_fail;
 
 /**
  * do_fw_call_smc() - Call system-level platform management layer (SMC)
  * @arg0:		Argument 0 to SMC call
  * @arg1:		Argument 1 to SMC call
  * @arg2:		Argument 2 to SMC call
+ * @arg3:		Argument 3 to SMC call
  * @ret_payload:	Returned value array
  *
  * Invoke platform management function via SMC call (no hypervisor present).
  *
  * Return: Returns status, either success or error+reason
  */
-static noinline int do_fw_call_smc(u64 arg0, u64 arg1, u64 arg2,
+static noinline int do_fw_call_smc(u64 arg0, u64 arg1, u64 arg2, u64 arg3,
 				   u32 *ret_payload)
 {
 	struct arm_smccc_res res;
 
-	arm_smccc_smc(arg0, arg1, arg2, 0, 0, 0, 0, 0, &res);
+	arm_smccc_smc(arg0, arg1, arg2, arg3, 0, 0, 0, 0, &res);
 
 	if (ret_payload) {
 		ret_payload[0] = lower_32_bits(res.a0);
@@ -158,6 +159,7 @@ static noinline int do_fw_call_smc(u64 arg0, u64 arg1, u64 arg2,
  * @arg0:		Argument 0 to HVC call
  * @arg1:		Argument 1 to HVC call
  * @arg2:		Argument 2 to HVC call
+ * @arg3:		Argument 3 to HVC call
  * @ret_payload:	Returned value array
  *
  * Invoke platform management function via HVC
@@ -166,12 +168,12 @@ static noinline int do_fw_call_smc(u64 arg0, u64 arg1, u64 arg2,
  *
  * Return: Returns status, either success or error+reason
  */
-static noinline int do_fw_call_hvc(u64 arg0, u64 arg1, u64 arg2,
+static noinline int do_fw_call_hvc(u64 arg0, u64 arg1, u64 arg2, u64 arg3,
 				   u32 *ret_payload)
 {
 	struct arm_smccc_res res;
 
-	arm_smccc_hvc(arg0, arg1, arg2, 0, 0, 0, 0, 0, &res);
+	arm_smccc_hvc(arg0, arg1, arg2, arg3, 0, 0, 0, 0, &res);
 
 	if (ret_payload) {
 		ret_payload[0] = lower_32_bits(res.a0);
@@ -191,7 +193,7 @@ static int __do_feature_check_call(const u32 api_id, u32 *ret_payload)
 	smc_arg[0] = PM_SIP_SVC | PM_FEATURE_CHECK;
 	smc_arg[1] = api_id;
 
-	ret = do_fw_call(smc_arg[0], smc_arg[1], 0, ret_payload);
+	ret = do_fw_call(smc_arg[0], smc_arg[1], 0, 0, ret_payload);
 	if (ret)
 		ret = -EOPNOTSUPP;
 	else
@@ -306,6 +308,7 @@ EXPORT_SYMBOL_GPL(zynqmp_pm_is_function_supported);
  * @arg1:		Argument 1 to requested PM-API call
  * @arg2:		Argument 2 to requested PM-API call
  * @arg3:		Argument 3 to requested PM-API call
+ * @arg4:		Argument 4 to requested PM-API call
  * @ret_payload:	Returned value array
  *
  * Invoke platform management function for SMC or HVC call, depending on
@@ -324,7 +327,8 @@ EXPORT_SYMBOL_GPL(zynqmp_pm_is_function_supported);
  * Return: Returns status, either success or error+reason
  */
 int zynqmp_pm_invoke_fn(u32 pm_api_id, u32 arg0, u32 arg1,
-			u32 arg2, u32 arg3, u32 *ret_payload)
+			u32 arg2, u32 arg3, u32 arg4,
+			u32 *ret_payload)
 {
 	/*
 	 * Added SIP service call Function Identifier
@@ -341,8 +345,10 @@ int zynqmp_pm_invoke_fn(u32 pm_api_id, u32 arg0, u32 arg1,
 	smc_arg[0] = PM_SIP_SVC | pm_api_id;
 	smc_arg[1] = ((u64)arg1 << 32) | arg0;
 	smc_arg[2] = ((u64)arg3 << 32) | arg2;
+	smc_arg[3] = ((u64)arg4);
 
-	return do_fw_call(smc_arg[0], smc_arg[1], smc_arg[2], ret_payload);
+	return do_fw_call(smc_arg[0], smc_arg[1], smc_arg[2], smc_arg[3],
+			  ret_payload);
 }
 
 static u32 pm_api_version;
@@ -353,13 +359,13 @@ int zynqmp_pm_register_sgi(u32 sgi_num, u32 reset)
 	int ret;
 
 	ret = zynqmp_pm_invoke_fn(TF_A_PM_REGISTER_SGI, sgi_num, reset, 0, 0,
-				  NULL);
+				  0, NULL);
 	if (!ret)
 		return ret;
 
 	/* try old implementation as fallback strategy if above fails */
 	return zynqmp_pm_invoke_fn(PM_IOCTL, 0, IOCTL_REGISTER_SGI, sgi_num,
-				   reset, NULL);
+				   reset, 0, NULL);
 }
 
 /**
@@ -381,7 +387,7 @@ int zynqmp_pm_get_api_version(u32 *version)
 		*version = pm_api_version;
 		return 0;
 	}
-	ret = zynqmp_pm_invoke_fn(PM_GET_API_VERSION, 0, 0, 0, 0, ret_payload);
+	ret = zynqmp_pm_invoke_fn(PM_GET_API_VERSION, 0, 0, 0, 0, 0, ret_payload);
 	*version = ret_payload[1];
 
 	return ret;
@@ -404,7 +410,7 @@ int zynqmp_pm_get_chipid(u32 *idcode, u32 *version)
 	if (!idcode || !version)
 		return -EINVAL;
 
-	ret = zynqmp_pm_invoke_fn(PM_GET_CHIPID, 0, 0, 0, 0, ret_payload);
+	ret = zynqmp_pm_invoke_fn(PM_GET_CHIPID, 0, 0, 0, 0, 0, ret_payload);
 	*idcode = ret_payload[1];
 	*version = ret_payload[2];
 
@@ -432,7 +438,7 @@ static int zynqmp_pm_get_trustzone_version(u32 *version)
 		return 0;
 	}
 	ret = zynqmp_pm_invoke_fn(PM_GET_TRUSTZONE_VERSION, 0, 0,
-				  0, 0, ret_payload);
+				  0, 0, 0, ret_payload);
 	*version = ret_payload[1];
 
 	return ret;
@@ -480,7 +486,7 @@ int zynqmp_pm_query_data(struct zynqmp_pm_query_data qdata, u32 *out)
 	int ret;
 
 	ret = zynqmp_pm_invoke_fn(PM_QUERY_DATA, qdata.qid, qdata.arg1,
-				  qdata.arg2, qdata.arg3, out);
+				  qdata.arg2, qdata.arg3, 0, out);
 
 	/*
 	 * For clock name query, all bytes in SMC response are clock name
@@ -502,7 +508,8 @@ EXPORT_SYMBOL_GPL(zynqmp_pm_query_data);
  */
 int zynqmp_pm_clock_enable(u32 clock_id)
 {
-	return zynqmp_pm_invoke_fn(PM_CLOCK_ENABLE, clock_id, 0, 0, 0, NULL);
+	return zynqmp_pm_invoke_fn(PM_CLOCK_ENABLE, clock_id, 0, 0, 0, 0,
+				   NULL);
 }
 EXPORT_SYMBOL_GPL(zynqmp_pm_clock_enable);
 
@@ -517,7 +524,8 @@ EXPORT_SYMBOL_GPL(zynqmp_pm_clock_enable);
  */
 int zynqmp_pm_clock_disable(u32 clock_id)
 {
-	return zynqmp_pm_invoke_fn(PM_CLOCK_DISABLE, clock_id, 0, 0, 0, NULL);
+	return zynqmp_pm_invoke_fn(PM_CLOCK_DISABLE, clock_id, 0, 0, 0, 0,
+				   NULL);
 }
 EXPORT_SYMBOL_GPL(zynqmp_pm_clock_disable);
 
@@ -537,7 +545,7 @@ int zynqmp_pm_clock_getstate(u32 clock_id, u32 *state)
 	int ret;
 
 	ret = zynqmp_pm_invoke_fn(PM_CLOCK_GETSTATE, clock_id, 0,
-				  0, 0, ret_payload);
+				  0, 0, 0, ret_payload);
 	*state = ret_payload[1];
 
 	return ret;
@@ -557,7 +565,7 @@ EXPORT_SYMBOL_GPL(zynqmp_pm_clock_getstate);
 int zynqmp_pm_clock_setdivider(u32 clock_id, u32 divider)
 {
 	return zynqmp_pm_invoke_fn(PM_CLOCK_SETDIVIDER, clock_id, divider,
-				   0, 0, NULL);
+				   0, 0, 0, NULL);
 }
 EXPORT_SYMBOL_GPL(zynqmp_pm_clock_setdivider);
 
@@ -577,7 +585,7 @@ int zynqmp_pm_clock_getdivider(u32 clock_id, u32 *divider)
 	int ret;
 
 	ret = zynqmp_pm_invoke_fn(PM_CLOCK_GETDIVIDER, clock_id, 0,
-				  0, 0, ret_payload);
+				  0, 0, 0, ret_payload);
 	*divider = ret_payload[1];
 
 	return ret;
@@ -598,7 +606,7 @@ int zynqmp_pm_clock_setrate(u32 clock_id, u64 rate)
 	return zynqmp_pm_invoke_fn(PM_CLOCK_SETRATE, clock_id,
 				   lower_32_bits(rate),
 				   upper_32_bits(rate),
-				   0, NULL);
+				   0, 0, NULL);
 }
 EXPORT_SYMBOL_GPL(zynqmp_pm_clock_setrate);
 
@@ -618,7 +626,7 @@ int zynqmp_pm_clock_getrate(u32 clock_id, u64 *rate)
 	int ret;
 
 	ret = zynqmp_pm_invoke_fn(PM_CLOCK_GETRATE, clock_id, 0,
-				  0, 0, ret_payload);
+				  0, 0, 0, ret_payload);
 	*rate = ((u64)ret_payload[2] << 32) | ret_payload[1];
 
 	return ret;
@@ -637,7 +645,7 @@ EXPORT_SYMBOL_GPL(zynqmp_pm_clock_getrate);
 int zynqmp_pm_clock_setparent(u32 clock_id, u32 parent_id)
 {
 	return zynqmp_pm_invoke_fn(PM_CLOCK_SETPARENT, clock_id,
-				   parent_id, 0, 0, NULL);
+				   parent_id, 0, 0, 0, NULL);
 }
 EXPORT_SYMBOL_GPL(zynqmp_pm_clock_setparent);
 
@@ -657,7 +665,7 @@ int zynqmp_pm_clock_getparent(u32 clock_id, u32 *parent_id)
 	int ret;
 
 	ret = zynqmp_pm_invoke_fn(PM_CLOCK_GETPARENT, clock_id, 0,
-				  0, 0, ret_payload);
+				  0, 0, 0, ret_payload);
 	*parent_id = ret_payload[1];
 
 	return ret;
@@ -677,7 +685,7 @@ EXPORT_SYMBOL_GPL(zynqmp_pm_clock_getparent);
 int zynqmp_pm_set_pll_frac_mode(u32 clk_id, u32 mode)
 {
 	return zynqmp_pm_invoke_fn(PM_IOCTL, 0, IOCTL_SET_PLL_FRAC_MODE,
-				   clk_id, mode, NULL);
+				   clk_id, mode, 0, NULL);
 }
 EXPORT_SYMBOL_GPL(zynqmp_pm_set_pll_frac_mode);
 
@@ -694,7 +702,7 @@ EXPORT_SYMBOL_GPL(zynqmp_pm_set_pll_frac_mode);
 int zynqmp_pm_get_pll_frac_mode(u32 clk_id, u32 *mode)
 {
 	return zynqmp_pm_invoke_fn(PM_IOCTL, 0, IOCTL_GET_PLL_FRAC_MODE,
-				   clk_id, 0, mode);
+				   clk_id, 0, 0, mode);
 }
 EXPORT_SYMBOL_GPL(zynqmp_pm_get_pll_frac_mode);
 
@@ -712,7 +720,7 @@ EXPORT_SYMBOL_GPL(zynqmp_pm_get_pll_frac_mode);
 int zynqmp_pm_set_pll_frac_data(u32 clk_id, u32 data)
 {
 	return zynqmp_pm_invoke_fn(PM_IOCTL, 0, IOCTL_SET_PLL_FRAC_DATA,
-				   clk_id, data, NULL);
+				   clk_id, data, 0, NULL);
 }
 EXPORT_SYMBOL_GPL(zynqmp_pm_set_pll_frac_data);
 
@@ -729,7 +737,7 @@ EXPORT_SYMBOL_GPL(zynqmp_pm_set_pll_frac_data);
 int zynqmp_pm_get_pll_frac_data(u32 clk_id, u32 *data)
 {
 	return zynqmp_pm_invoke_fn(PM_IOCTL, 0, IOCTL_GET_PLL_FRAC_DATA,
-				   clk_id, 0, data);
+				   clk_id, 0, 0, data);
 }
 EXPORT_SYMBOL_GPL(zynqmp_pm_get_pll_frac_data);
 
@@ -747,7 +755,7 @@ EXPORT_SYMBOL_GPL(zynqmp_pm_get_pll_frac_data);
 int zynqmp_pm_set_sd_tapdelay(u32 node_id, u32 type, u32 value)
 {
 	return zynqmp_pm_invoke_fn(PM_IOCTL, node_id, IOCTL_SET_SD_TAPDELAY,
-				   type, value, NULL);
+				   type, value, 0, NULL);
 }
 EXPORT_SYMBOL_GPL(zynqmp_pm_set_sd_tapdelay);
 
@@ -764,7 +772,7 @@ EXPORT_SYMBOL_GPL(zynqmp_pm_set_sd_tapdelay);
 int zynqmp_pm_sd_dll_reset(u32 node_id, u32 type)
 {
 	return zynqmp_pm_invoke_fn(PM_IOCTL, node_id, IOCTL_SD_DLL_RESET,
-				   type, 0, NULL);
+				   type, 0, 0, NULL);
 }
 EXPORT_SYMBOL_GPL(zynqmp_pm_sd_dll_reset);
 
@@ -781,7 +789,7 @@ EXPORT_SYMBOL_GPL(zynqmp_pm_sd_dll_reset);
 int zynqmp_pm_ospi_mux_select(u32 dev_id, u32 select)
 {
 	return zynqmp_pm_invoke_fn(PM_IOCTL, dev_id, IOCTL_OSPI_MUX_SELECT,
-				   select, 0, NULL);
+				   select, 0, 0, NULL);
 }
 EXPORT_SYMBOL_GPL(zynqmp_pm_ospi_mux_select);
 
@@ -797,7 +805,7 @@ EXPORT_SYMBOL_GPL(zynqmp_pm_ospi_mux_select);
 int zynqmp_pm_write_ggs(u32 index, u32 value)
 {
 	return zynqmp_pm_invoke_fn(PM_IOCTL, 0, IOCTL_WRITE_GGS,
-				   index, value, NULL);
+				   index, value, 0, NULL);
 }
 EXPORT_SYMBOL_GPL(zynqmp_pm_write_ggs);
 
@@ -813,7 +821,7 @@ EXPORT_SYMBOL_GPL(zynqmp_pm_write_ggs);
 int zynqmp_pm_read_ggs(u32 index, u32 *value)
 {
 	return zynqmp_pm_invoke_fn(PM_IOCTL, 0, IOCTL_READ_GGS,
-				   index, 0, value);
+				   index, 0, 0, value);
 }
 EXPORT_SYMBOL_GPL(zynqmp_pm_read_ggs);
 
@@ -830,7 +838,7 @@ EXPORT_SYMBOL_GPL(zynqmp_pm_read_ggs);
 int zynqmp_pm_write_pggs(u32 index, u32 value)
 {
 	return zynqmp_pm_invoke_fn(PM_IOCTL, 0, IOCTL_WRITE_PGGS, index, value,
-				   NULL);
+				   0, NULL);
 }
 EXPORT_SYMBOL_GPL(zynqmp_pm_write_pggs);
 
@@ -847,33 +855,34 @@ EXPORT_SYMBOL_GPL(zynqmp_pm_write_pggs);
 int zynqmp_pm_read_pggs(u32 index, u32 *value)
 {
 	return zynqmp_pm_invoke_fn(PM_IOCTL, 0, IOCTL_READ_PGGS, index, 0,
-				   value);
+				   0, value);
 }
 EXPORT_SYMBOL_GPL(zynqmp_pm_read_pggs);
 
 int zynqmp_pm_set_tapdelay_bypass(u32 index, u32 value)
 {
 	return zynqmp_pm_invoke_fn(PM_IOCTL, 0, IOCTL_SET_TAPDELAY_BYPASS,
-				   index, value, NULL);
+				   index, value, 0, NULL);
 }
 EXPORT_SYMBOL_GPL(zynqmp_pm_set_tapdelay_bypass);
 
 int zynqmp_pm_usb_set_state(u32 node, u32 state, u32 value)
 {
 	return zynqmp_pm_invoke_fn(PM_IOCTL, node, IOCTL_USB_SET_STATE, state,
-				   value, NULL);
+				   value, 0, NULL);
 }
 EXPORT_SYMBOL_GPL(zynqmp_pm_usb_set_state);
 
 int zynqmp_pm_ulpi_reset(void)
 {
-	return zynqmp_pm_invoke_fn(PM_IOCTL, 0, IOCTL_ULPI_RESET, 0, 0, NULL);
+	return zynqmp_pm_invoke_fn(PM_IOCTL, 0, IOCTL_ULPI_RESET, 0, 0, 0,
+				   NULL);
 }
 EXPORT_SYMBOL_GPL(zynqmp_pm_ulpi_reset);
 
 int zynqmp_pm_afi(u32 index, u32 value)
 {
-	return zynqmp_pm_invoke_fn(PM_IOCTL, 0, IOCTL_AFI, index, value,
+	return zynqmp_pm_invoke_fn(PM_IOCTL, 0, IOCTL_AFI, index, value, 0,
 				   NULL);
 }
 EXPORT_SYMBOL_GPL(zynqmp_pm_afi);
@@ -881,28 +890,28 @@ EXPORT_SYMBOL_GPL(zynqmp_pm_afi);
 int zynqmp_pm_set_sgmii_mode(u32 enable)
 {
 	return zynqmp_pm_invoke_fn(PM_IOCTL, 0, IOCTL_SET_SGMII_MODE, enable, 0,
-				   NULL);
+				   0, NULL);
 }
 EXPORT_SYMBOL_GPL(zynqmp_pm_set_sgmii_mode);
 
 int zynqmp_pm_probe_counter_read(u32 deviceid, u32 reg, u32 *value)
 {
 	return zynqmp_pm_invoke_fn(PM_IOCTL, deviceid, IOCTL_PROBE_COUNTER_READ, reg,
-				   0, value);
+				   0, 0, value);
 }
 EXPORT_SYMBOL_GPL(zynqmp_pm_probe_counter_read);
 
 int zynqmp_pm_probe_counter_write(u32 domain, u32 reg, u32 value)
 {
 	return zynqmp_pm_invoke_fn(PM_IOCTL, domain, IOCTL_PROBE_COUNTER_WRITE, reg,
-				   value, NULL);
+				   value, 0, NULL);
 }
 EXPORT_SYMBOL_GPL(zynqmp_pm_probe_counter_write);
 
 int zynqmp_pm_get_last_reset_reason(u32 *reset_reason)
 {
 	return zynqmp_pm_invoke_fn(PM_IOCTL, 0, IOCTL_GET_LAST_RESET_REASON, 0,
-				   0, reset_reason);
+				   0, 0, reset_reason);
 }
 EXPORT_SYMBOL_GPL(zynqmp_pm_get_last_reset_reason);
 
@@ -918,7 +927,7 @@ EXPORT_SYMBOL_GPL(zynqmp_pm_get_last_reset_reason);
 int zynqmp_pm_set_boot_health_status(u32 value)
 {
 	return zynqmp_pm_invoke_fn(PM_IOCTL, 0, IOCTL_SET_BOOT_HEALTH_STATUS,
-				   value, 0, NULL);
+				   value, 0, 0, NULL);
 }
 
 /**
@@ -933,7 +942,7 @@ int zynqmp_pm_reset_assert(const u32 reset,
 			   const enum zynqmp_pm_reset_action assert_flag)
 {
 	return zynqmp_pm_invoke_fn(PM_RESET_ASSERT, reset, assert_flag,
-				   0, 0, NULL);
+				   0, 0, 0, NULL);
 }
 EXPORT_SYMBOL_GPL(zynqmp_pm_reset_assert);
 
@@ -953,7 +962,7 @@ int zynqmp_pm_reset_get_status(const u32 reset, u32 *status)
 		return -EINVAL;
 
 	ret = zynqmp_pm_invoke_fn(PM_RESET_GET_STATUS, reset, 0,
-				  0, 0, ret_payload);
+				  0, 0, 0, ret_payload);
 	*status = ret_payload[1];
 
 	return ret;
@@ -976,7 +985,8 @@ EXPORT_SYMBOL_GPL(zynqmp_pm_reset_get_status);
 int zynqmp_pm_fpga_load(const u64 address, const u32 size, const u32 flags)
 {
 	return zynqmp_pm_invoke_fn(PM_FPGA_LOAD, lower_32_bits(address),
-				   upper_32_bits(address), size, flags, NULL);
+				   upper_32_bits(address), size, flags, 0,
+				   NULL);
 }
 EXPORT_SYMBOL_GPL(zynqmp_pm_fpga_load);
 
@@ -997,7 +1007,8 @@ int zynqmp_pm_fpga_get_status(u32 *value)
 	if (!value)
 		return -EINVAL;
 
-	ret = zynqmp_pm_invoke_fn(PM_FPGA_GET_STATUS, 0, 0, 0, 0, ret_payload);
+	ret = zynqmp_pm_invoke_fn(PM_FPGA_GET_STATUS, 0, 0, 0, 0, 0,
+				  ret_payload);
 	*value = ret_payload[1];
 
 	return ret;
@@ -1014,7 +1025,7 @@ EXPORT_SYMBOL_GPL(zynqmp_pm_fpga_get_status);
  */
 int zynqmp_pm_pinctrl_request(const u32 pin)
 {
-	return zynqmp_pm_invoke_fn(PM_PINCTRL_REQUEST, pin, 0, 0, 0, NULL);
+	return zynqmp_pm_invoke_fn(PM_PINCTRL_REQUEST, pin, 0, 0, 0, 0, NULL);
 }
 EXPORT_SYMBOL_GPL(zynqmp_pm_pinctrl_request);
 
@@ -1028,7 +1039,7 @@ EXPORT_SYMBOL_GPL(zynqmp_pm_pinctrl_request);
  */
 int zynqmp_pm_pinctrl_release(const u32 pin)
 {
-	return zynqmp_pm_invoke_fn(PM_PINCTRL_RELEASE, pin, 0, 0, 0, NULL);
+	return zynqmp_pm_invoke_fn(PM_PINCTRL_RELEASE, pin, 0, 0, 0, 0, NULL);
 }
 EXPORT_SYMBOL_GPL(zynqmp_pm_pinctrl_release);
 
@@ -1050,7 +1061,7 @@ int zynqmp_pm_pinctrl_get_function(const u32 pin, u32 *id)
 		return -EINVAL;
 
 	ret = zynqmp_pm_invoke_fn(PM_PINCTRL_GET_FUNCTION, pin, 0,
-				  0, 0, ret_payload);
+				  0, 0, 0, ret_payload);
 	*id = ret_payload[1];
 
 	return ret;
@@ -1069,7 +1080,7 @@ EXPORT_SYMBOL_GPL(zynqmp_pm_pinctrl_get_function);
 int zynqmp_pm_pinctrl_set_function(const u32 pin, const u32 id)
 {
 	return zynqmp_pm_invoke_fn(PM_PINCTRL_SET_FUNCTION, pin, id,
-				   0, 0, NULL);
+				   0, 0, 0, NULL);
 }
 EXPORT_SYMBOL_GPL(zynqmp_pm_pinctrl_set_function);
 
@@ -1093,7 +1104,7 @@ int zynqmp_pm_pinctrl_get_config(const u32 pin, const u32 param,
 		return -EINVAL;
 
 	ret = zynqmp_pm_invoke_fn(PM_PINCTRL_CONFIG_PARAM_GET, pin, param,
-				  0, 0, ret_payload);
+				  0, 0, 0, ret_payload);
 	*value = ret_payload[1];
 
 	return ret;
@@ -1114,7 +1125,7 @@ int zynqmp_pm_pinctrl_set_config(const u32 pin, const u32 param,
 				 u32 value)
 {
 	return zynqmp_pm_invoke_fn(PM_PINCTRL_CONFIG_PARAM_SET, pin,
-				   param, value, 0, NULL);
+				   param, value, 0, 0, NULL);
 }
 EXPORT_SYMBOL_GPL(zynqmp_pm_pinctrl_set_config);
 
@@ -1133,7 +1144,7 @@ unsigned int zynqmp_pm_bootmode_read(u32 *ps_mode)
 	u32 ret_payload[PAYLOAD_ARG_CNT];
 
 	ret = zynqmp_pm_invoke_fn(PM_MMIO_READ, CRL_APB_BOOT_PIN_CTRL, 0,
-				  0, 0, ret_payload);
+				  0, 0, 0, ret_payload);
 
 	*ps_mode = ret_payload[1];
 
@@ -1153,7 +1164,8 @@ EXPORT_SYMBOL_GPL(zynqmp_pm_bootmode_read);
 int zynqmp_pm_bootmode_write(u32 ps_mode)
 {
 	return zynqmp_pm_invoke_fn(PM_MMIO_WRITE, CRL_APB_BOOT_PIN_CTRL,
-				   CRL_APB_BOOTPIN_CTRL_MASK, ps_mode, 0, NULL);
+				   CRL_APB_BOOTPIN_CTRL_MASK, ps_mode, 0,
+				   0,  NULL);
 }
 EXPORT_SYMBOL_GPL(zynqmp_pm_bootmode_write);
 
@@ -1168,7 +1180,7 @@ EXPORT_SYMBOL_GPL(zynqmp_pm_bootmode_write);
  */
 int zynqmp_pm_init_finalize(void)
 {
-	return zynqmp_pm_invoke_fn(PM_PM_INIT_FINALIZE, 0, 0, 0, 0, NULL);
+	return zynqmp_pm_invoke_fn(PM_PM_INIT_FINALIZE, 0, 0, 0, 0, 0, NULL);
 }
 EXPORT_SYMBOL_GPL(zynqmp_pm_init_finalize);
 
@@ -1189,7 +1201,7 @@ int zynqmp_pm_write_aes_key(const u32 keylen, const u32 keysrc,
 {
 	return zynqmp_pm_invoke_fn(PM_WRITE_AES_KEY, keylen, keysrc,
 				   lower_32_bits(keyaddr),
-				   upper_32_bits(keyaddr), NULL);
+				   upper_32_bits(keyaddr), 0, NULL);
 }
 EXPORT_SYMBOL_GPL(zynqmp_pm_write_aes_key);
 
@@ -1207,7 +1219,7 @@ int zynqmp_pm_bbram_write_aeskey(u32 keylen, const u64 keyaddr)
 {
 	return zynqmp_pm_invoke_fn(PM_BBRAM_WRITE_KEY, keylen,
 				   lower_32_bits(keyaddr),
-				   upper_32_bits(keyaddr), 0, NULL);
+				   upper_32_bits(keyaddr), 0, 0, NULL);
 }
 EXPORT_SYMBOL_GPL(zynqmp_pm_bbram_write_aeskey);
 
@@ -1222,7 +1234,7 @@ EXPORT_SYMBOL_GPL(zynqmp_pm_bbram_write_aeskey);
  */
 int zynqmp_pm_bbram_write_usrdata(u32 data)
 {
-	return zynqmp_pm_invoke_fn(PM_BBRAM_WRITE_USERDATA, data, 0, 0, 0,
+	return zynqmp_pm_invoke_fn(PM_BBRAM_WRITE_USERDATA, data, 0, 0, 0, 0,
 				   NULL);
 }
 EXPORT_SYMBOL_GPL(zynqmp_pm_bbram_write_usrdata);
@@ -1237,7 +1249,7 @@ EXPORT_SYMBOL_GPL(zynqmp_pm_bbram_write_usrdata);
  */
 int zynqmp_pm_bbram_read_usrdata(const u64 outaddr)
 {
-	return zynqmp_pm_invoke_fn(PM_BBRAM_READ_USERDATA, outaddr, 0, 0, 0,
+	return zynqmp_pm_invoke_fn(PM_BBRAM_READ_USERDATA, outaddr, 0, 0, 0, 0,
 				   NULL);
 }
 EXPORT_SYMBOL_GPL(zynqmp_pm_bbram_read_usrdata);
@@ -1252,7 +1264,7 @@ EXPORT_SYMBOL_GPL(zynqmp_pm_bbram_read_usrdata);
  */
 int zynqmp_pm_bbram_zeroize(void)
 {
-	return zynqmp_pm_invoke_fn(PM_BBRAM_ZEROIZE, 0, 0, 0, 0, NULL);
+	return zynqmp_pm_invoke_fn(PM_BBRAM_ZEROIZE, 0, 0, 0, 0, 0, NULL);
 }
 EXPORT_SYMBOL_GPL(zynqmp_pm_bbram_zeroize);
 
@@ -1266,7 +1278,7 @@ EXPORT_SYMBOL_GPL(zynqmp_pm_bbram_zeroize);
  */
 int zynqmp_pm_bbram_lock_userdata(void)
 {
-	return zynqmp_pm_invoke_fn(PM_BBRAM_LOCK_USERDATA, 0, 0, 0, 0, NULL);
+	return zynqmp_pm_invoke_fn(PM_BBRAM_LOCK_USERDATA, 0, 0, 0, 0, 0, NULL);
 }
 EXPORT_SYMBOL_GPL(zynqmp_pm_bbram_lock_userdata);
 
@@ -1291,7 +1303,7 @@ int zynqmp_pm_get_uid_info(const u64 address, const u32 size, u32 *count)
 	ret = zynqmp_pm_invoke_fn(PM_GET_UID_INFO_LIST,
 				  upper_32_bits(address),
 				  lower_32_bits(address),
-				  size, 0, ret_payload);
+				  size, 0, 0, ret_payload);
 
 	*count = ret_payload[1];
 
@@ -1323,7 +1335,7 @@ int zynqmp_pm_fpga_read(const u32 reg_numframes, const u64 phys_address,
 	ret = zynqmp_pm_invoke_fn(PM_FPGA_READ, reg_numframes,
 				  lower_32_bits(phys_address),
 				  upper_32_bits(phys_address), readback_type,
-				  ret_payload);
+				  0, ret_payload);
 	*value = ret_payload[1];
 
 	return ret;
@@ -1345,11 +1357,10 @@ int zynqmp_pm_request_suspend(const u32 node,
 			      const u32 latency, const u32 state)
 {
 	return zynqmp_pm_invoke_fn(PM_REQUEST_SUSPEND, node, ack,
-				   latency, state, NULL);
+				   latency, state, 0, NULL);
 }
 EXPORT_SYMBOL_GPL(zynqmp_pm_request_suspend);
 
-
 /**
  * zynqmp_pm_set_wakeup_source - PM call to specify the wakeup source
  *					while suspended
@@ -1363,7 +1374,7 @@ int zynqmp_pm_set_wakeup_source(const u32 target, const u32 wakeup_node,
 				const u32 enable)
 {
 	return zynqmp_pm_invoke_fn(PM_SET_WAKEUP_SOURCE, target,
-				   wakeup_node, enable, 0, NULL);
+				   wakeup_node, enable, 0, 0, NULL);
 }
 EXPORT_SYMBOL_GPL(zynqmp_pm_set_wakeup_source);
 
@@ -1377,7 +1388,7 @@ EXPORT_SYMBOL_GPL(zynqmp_pm_set_wakeup_source);
 int zynqmp_pm_set_max_latency(const u32 node, const u32 latency)
 {
 	return zynqmp_pm_invoke_fn(PM_SET_MAX_LATENCY, node, latency,
-				   0, 0, NULL);
+				   0, 0, 0, NULL);
 }
 EXPORT_SYMBOL_GPL(zynqmp_pm_set_max_latency);
 
@@ -1390,7 +1401,7 @@ EXPORT_SYMBOL_GPL(zynqmp_pm_set_max_latency);
 int zynqmp_pm_set_configuration(const u32 physical_addr)
 {
 	return zynqmp_pm_invoke_fn(PM_SET_CONFIGURATION, physical_addr, 0,
-				   0, 0, NULL);
+				   0, 0, 0, NULL);
 }
 EXPORT_SYMBOL_GPL(zynqmp_pm_set_configuration);
 
@@ -1423,7 +1434,7 @@ int zynqmp_pm_get_node_status(const u32 node, u32 *const status,
 		return -EINVAL;
 
 	ret = zynqmp_pm_invoke_fn(PM_GET_NODE_STATUS, node, 0, 0,
-				  0, ret_payload);
+				  0, 0, ret_payload);
 	if (ret_payload[0] == XST_PM_SUCCESS) {
 		*status = ret_payload[1];
 		if (requirements)
@@ -1456,7 +1467,7 @@ int zynqmp_pm_get_operating_characteristic(const u32 node,
 		return -EINVAL;
 
 	ret = zynqmp_pm_invoke_fn(PM_GET_OPERATING_CHARACTERISTIC,
-				  node, type, 0, 0, ret_payload);
+				  node, type, 0, 0, 0, ret_payload);
 	if (ret_payload[0] == XST_PM_SUCCESS)
 		*result = ret_payload[1];
 
@@ -1480,7 +1491,7 @@ int zynqmp_pm_config_reg_access(u32 register_access_id, u32 address,
 				u32 mask, u32 value, u32 *out)
 {
 	return zynqmp_pm_invoke_fn(PM_REGISTER_ACCESS, register_access_id,
-				   address, mask, value, out);
+				   address, mask, value, 0, out);
 }
 EXPORT_SYMBOL_GPL(zynqmp_pm_config_reg_access);
 
@@ -1499,7 +1510,7 @@ int zynqmp_pm_mmio_read(u32 address, u32 *out)
 	u32 ret_payload[PAYLOAD_ARG_CNT];
 	int ret;
 
-	ret = zynqmp_pm_invoke_fn(PM_MMIO_READ, address, 0, 0, 0,
+	ret = zynqmp_pm_invoke_fn(PM_MMIO_READ, address, 0, 0, 0, 0,
 				  ret_payload);
 	*out = ret_payload[1];
 
@@ -1521,7 +1532,7 @@ EXPORT_SYMBOL_GPL(zynqmp_pm_mmio_read);
 int zynqmp_pm_mmio_write(u32 address, u32 mask, u32 value)
 {
 	return zynqmp_pm_invoke_fn(PM_MMIO_WRITE, address, mask,
-				   value, 0, NULL);
+				   value, 0, 0, NULL);
 }
 EXPORT_SYMBOL_GPL(zynqmp_pm_mmio_write);
 
@@ -1535,7 +1546,7 @@ EXPORT_SYMBOL_GPL(zynqmp_pm_mmio_write);
  */
 int zynqmp_pm_set_suspend_mode(u32 mode)
 {
-	return zynqmp_pm_invoke_fn(PM_SET_SUSPEND_MODE, mode, 0, 0, 0, NULL);
+	return zynqmp_pm_invoke_fn(PM_SET_SUSPEND_MODE, mode, 0, 0, 0, 0, NULL);
 }
 EXPORT_SYMBOL_GPL(zynqmp_pm_set_suspend_mode);
 
@@ -1555,7 +1566,7 @@ int zynqmp_pm_request_node(const u32 node, const u32 capabilities,
 			   const u32 qos, const enum zynqmp_pm_request_ack ack)
 {
 	return zynqmp_pm_invoke_fn(PM_REQUEST_NODE, node, capabilities,
-				   qos, ack, NULL);
+				   qos, ack, 0, NULL);
 }
 EXPORT_SYMBOL_GPL(zynqmp_pm_request_node);
 
@@ -1571,7 +1582,7 @@ EXPORT_SYMBOL_GPL(zynqmp_pm_request_node);
  */
 int zynqmp_pm_release_node(const u32 node)
 {
-	return zynqmp_pm_invoke_fn(PM_RELEASE_NODE, node, 0, 0, 0, NULL);
+	return zynqmp_pm_invoke_fn(PM_RELEASE_NODE, node, 0, 0, 0, 0, NULL);
 }
 EXPORT_SYMBOL_GPL(zynqmp_pm_release_node);
 
@@ -1591,7 +1602,8 @@ int zynqmp_pm_get_rpu_mode(u32 node_id, enum rpu_oper_mode *rpu_mode)
 	int ret;
 
 	ret = zynqmp_pm_invoke_fn(PM_IOCTL, node_id,
-				  IOCTL_GET_RPU_OPER_MODE, 0, 0, ret_payload);
+				  IOCTL_GET_RPU_OPER_MODE, 0, 0, 0,
+				  ret_payload);
 
 	/* only set rpu_mode if no error */
 	if (ret == XST_PM_SUCCESS)
@@ -1615,7 +1627,7 @@ int zynqmp_pm_set_rpu_mode(u32 node_id, enum rpu_oper_mode rpu_mode)
 {
 	return zynqmp_pm_invoke_fn(PM_IOCTL, node_id,
 				   IOCTL_SET_RPU_OPER_MODE, (u32)rpu_mode,
-				   0, NULL);
+				   0, 0, NULL);
 }
 EXPORT_SYMBOL_GPL(zynqmp_pm_set_rpu_mode);
 
@@ -1633,7 +1645,7 @@ int zynqmp_pm_set_tcm_config(u32 node_id, enum rpu_tcm_comb tcm_mode)
 {
 	return zynqmp_pm_invoke_fn(PM_IOCTL, node_id,
 				   IOCTL_TCM_COMB_CONFIG, (u32)tcm_mode, 0,
-				   NULL);
+				   0, NULL);
 }
 EXPORT_SYMBOL_GPL(zynqmp_pm_set_tcm_config);
 
@@ -1648,7 +1660,8 @@ EXPORT_SYMBOL_GPL(zynqmp_pm_set_tcm_config);
 int zynqmp_pm_force_pwrdwn(const u32 node,
 			   const enum zynqmp_pm_request_ack ack)
 {
-	return zynqmp_pm_invoke_fn(PM_FORCE_POWERDOWN, node, ack, 0, 0, NULL);
+	return zynqmp_pm_invoke_fn(PM_FORCE_POWERDOWN, node, ack, 0, 0, 0,
+				   NULL);
 }
 EXPORT_SYMBOL_GPL(zynqmp_pm_force_pwrdwn);
 
@@ -1668,7 +1681,7 @@ int zynqmp_pm_request_wake(const u32 node,
 {
 	/* set_addr flag is encoded into 1st bit of address */
 	return zynqmp_pm_invoke_fn(PM_REQUEST_WAKEUP, node, address | set_addr,
-				   address >> 32, ack, NULL);
+				   address >> 32, ack, 0, NULL);
 }
 EXPORT_SYMBOL_GPL(zynqmp_pm_request_wake);
 
@@ -1689,7 +1702,7 @@ int zynqmp_pm_set_requirement(const u32 node, const u32 capabilities,
 			      const enum zynqmp_pm_request_ack ack)
 {
 	return zynqmp_pm_invoke_fn(PM_SET_REQUIREMENT, node, capabilities,
-				   qos, ack, NULL);
+				   qos, ack, 0, NULL);
 }
 EXPORT_SYMBOL_GPL(zynqmp_pm_set_requirement);
 
@@ -1706,7 +1719,8 @@ int zynqmp_pm_load_pdi(const u32 src, const u64 address)
 {
 	return zynqmp_pm_invoke_fn(PM_LOAD_PDI, src,
 				   lower_32_bits(address),
-				   upper_32_bits(address), 0, NULL);
+				   upper_32_bits(address), 0,
+				   0, NULL);
 }
 EXPORT_SYMBOL_GPL(zynqmp_pm_load_pdi);
 
@@ -1728,7 +1742,7 @@ int zynqmp_pm_aes_engine(const u64 address, u32 *out)
 
 	ret = zynqmp_pm_invoke_fn(PM_SECURE_AES, upper_32_bits(address),
 				  lower_32_bits(address),
-				  0, 0, ret_payload);
+				  0, 0, 0, ret_payload);
 	*out = ret_payload[1];
 
 	return ret;
@@ -1751,7 +1765,8 @@ int zynqmp_pm_efuse_access(const u64 address, u32 *out)
 		return -EINVAL;
 
 	ret = zynqmp_pm_invoke_fn(PM_EFUSE_ACCESS, upper_32_bits(address),
-				  lower_32_bits(address), 0, 0, ret_payload);
+				  lower_32_bits(address), 0, 0, 0,
+				  ret_payload);
 	*out = ret_payload[1];
 
 	return ret;
@@ -1780,7 +1795,7 @@ int zynqmp_pm_sha_hash(const u64 address, const u32 size, const u32 flags)
 	u32 upper_addr = upper_32_bits(address);
 
 	return zynqmp_pm_invoke_fn(PM_SECURE_SHA, upper_addr, lower_addr,
-				   size, flags, NULL);
+				   size, flags, 0, NULL);
 }
 EXPORT_SYMBOL_GPL(zynqmp_pm_sha_hash);
 
@@ -1801,7 +1816,7 @@ int zynqmp_pm_rsa(const u64 address, const u32 size, const u32 flags)
 	u32 upper_32_bits = upper_32_bits(address);
 
 	return zynqmp_pm_invoke_fn(PM_SECURE_RSA, upper_32_bits, lower_32_bits,
-				   size, flags, NULL);
+				   size, flags, 0, NULL);
 }
 EXPORT_SYMBOL_GPL(zynqmp_pm_rsa);
 
@@ -1824,7 +1839,7 @@ int zynqmp_pm_register_notifier(const u32 node, const u32 event,
 				const u32 wake, const u32 enable)
 {
 	return zynqmp_pm_invoke_fn(PM_REGISTER_NOTIFIER, node, event,
-				   wake, enable, NULL);
+				   wake, enable, 0, NULL);
 }
 EXPORT_SYMBOL_GPL(zynqmp_pm_register_notifier);
 
@@ -1838,7 +1853,7 @@ EXPORT_SYMBOL_GPL(zynqmp_pm_register_notifier);
 int zynqmp_pm_system_shutdown(const u32 type, const u32 subtype)
 {
 	return zynqmp_pm_invoke_fn(PM_SYSTEM_SHUTDOWN, type, subtype,
-				   0, 0, NULL);
+				   0, 0, 0, NULL);
 }
 
 /**
@@ -1851,7 +1866,7 @@ int zynqmp_pm_system_shutdown(const u32 type, const u32 subtype)
 int zynqmp_pm_set_feature_config(enum pm_feature_config_id id, u32 value)
 {
 	return zynqmp_pm_invoke_fn(PM_IOCTL, 0, IOCTL_SET_FEATURE_CONFIG,
-				   id, value, NULL);
+				   id, value, 0, NULL);
 }
 
 /**
@@ -1865,7 +1880,7 @@ int zynqmp_pm_get_feature_config(enum pm_feature_config_id id,
 				 u32 *payload)
 {
 	return zynqmp_pm_invoke_fn(PM_IOCTL, 0, IOCTL_GET_FEATURE_CONFIG,
-				   id, 0, payload);
+				   id, 0, 0, payload);
 }
 
 /**
@@ -1879,7 +1894,7 @@ int zynqmp_pm_get_feature_config(enum pm_feature_config_id id,
 int zynqmp_pm_set_sd_config(u32 node, enum pm_sd_config_type config, u32 value)
 {
 	return zynqmp_pm_invoke_fn(PM_IOCTL, node, IOCTL_SET_SD_CONFIG,
-				   config, value, NULL);
+				   config, value, 0, NULL);
 }
 EXPORT_SYMBOL_GPL(zynqmp_pm_set_sd_config);
 
@@ -1895,7 +1910,7 @@ int zynqmp_pm_set_gem_config(u32 node, enum pm_gem_config_type config,
 			     u32 value)
 {
 	return zynqmp_pm_invoke_fn(PM_IOCTL, node, IOCTL_SET_GEM_CONFIG,
-				   config, value, NULL);
+				   config, value, 0, NULL);
 }
 EXPORT_SYMBOL_GPL(zynqmp_pm_set_gem_config);
 
diff --git a/drivers/soc/xilinx/xlnx_event_manager.c b/drivers/soc/xilinx/xlnx_event_manager.c
index 2de082765bef..90e560a1dc9a 100644
--- a/drivers/soc/xilinx/xlnx_event_manager.c
+++ b/drivers/soc/xilinx/xlnx_event_manager.c
@@ -479,7 +479,7 @@ static void xlnx_call_notify_cb_handler(const u32 *payload)
 
 static void xlnx_get_event_callback_data(u32 *buf)
 {
-	zynqmp_pm_invoke_fn(GET_CALLBACK_DATA, 0, 0, 0, 0, buf);
+	zynqmp_pm_invoke_fn(GET_CALLBACK_DATA, 0, 0, 0, 0, 0, buf);
 }
 
 static irqreturn_t xlnx_event_handler(int irq, void *dev_id)
diff --git a/drivers/soc/xilinx/zynqmp_power.c b/drivers/soc/xilinx/zynqmp_power.c
index 78a8a7545d1e..8468ffec0e29 100644
--- a/drivers/soc/xilinx/zynqmp_power.c
+++ b/drivers/soc/xilinx/zynqmp_power.c
@@ -50,7 +50,7 @@ static enum pm_suspend_mode suspend_mode = PM_SUSPEND_MODE_STD;
 
 static void zynqmp_pm_get_callback_data(u32 *buf)
 {
-	zynqmp_pm_invoke_fn(GET_CALLBACK_DATA, 0, 0, 0, 0, buf);
+	zynqmp_pm_invoke_fn(GET_CALLBACK_DATA, 0, 0, 0, 0, 0, buf);
 }
 
 static void suspend_event_callback(const u32 *payload, void *data)
diff --git a/include/linux/firmware/xlnx-zynqmp.h b/include/linux/firmware/xlnx-zynqmp.h
index 98311e428253..e703249738f2 100644
--- a/include/linux/firmware/xlnx-zynqmp.h
+++ b/include/linux/firmware/xlnx-zynqmp.h
@@ -565,7 +565,7 @@ struct zynqmp_pm_query_data {
 };
 
 int zynqmp_pm_invoke_fn(u32 pm_api_id, u32 arg0, u32 arg1,
-			u32 arg2, u32 arg3, u32 *ret_payload);
+			u32 arg2, u32 arg3, u32 arg4, u32 *ret_payload);
 
 #if IS_REACHABLE(CONFIG_ZYNQMP_FIRMWARE)
 int zynqmp_pm_get_api_version(u32 *version);
-- 
2.34.1

