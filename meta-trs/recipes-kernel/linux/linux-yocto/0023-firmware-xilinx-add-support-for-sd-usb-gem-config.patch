Upstream-Status: Pending
From 62f6056163e4a1d1f7d5ba99c72fa3425eae8371 Mon Sep 17 00:00:00 2001
From: Rajan Vaja <rajan.vaja@xilinx.com>
Date: Wed, 8 Dec 2021 22:30:59 -0800
Subject: [PATCH 023/295] firmware: xilinx: add support for sd/usb/gem config

Add new APIs in firmware to configure SD/GEM/USB registers. Internally
it calls PM IOCTL for below SD/GEM/USB register configuration:
 - SD/EMMC select
 - SD slot type
 - SD base clock
 - SD 8 bit support
 - GEM SGMII Mode
 - SD fixed config
 - GEM fixed config
 - USB fixed config

Signed-off-by: Rajan Vaja <rajan.vaja@xilinx.com>
Signed-off-by: Radhey Shyam Pandey <radhey.shyam.pandey@xilinx.com>
State: pending
[michals:
Only USB now. SD/GEM was upstreamed already
]
---
 drivers/firmware/xilinx/zynqmp.c     | 16 ++++++++++++++++
 include/linux/firmware/xlnx-zynqmp.h | 14 ++++++++++++++
 2 files changed, 30 insertions(+)

diff --git a/drivers/firmware/xilinx/zynqmp.c b/drivers/firmware/xilinx/zynqmp.c
index 206b2044b07b..99c0ddba353a 100644
--- a/drivers/firmware/xilinx/zynqmp.c
+++ b/drivers/firmware/xilinx/zynqmp.c
@@ -1960,6 +1960,22 @@ int zynqmp_pm_set_gem_config(u32 node, enum pm_gem_config_type config,
 }
 EXPORT_SYMBOL_GPL(zynqmp_pm_set_gem_config);
 
+/**
+ * zynqmp_pm_set_usb_config - PM call to set value of USB config registers
+ * @node:	USB node ID
+ * @config:	The config type of USB registers
+ * @value:	Value to be set
+ *
+ * Return:      Returns 0 on success or error value on failure.
+ */
+int zynqmp_pm_set_usb_config(u32 node, enum pm_usb_config_type config,
+			     u32 value)
+{
+	return zynqmp_pm_invoke_fn(PM_IOCTL, node, IOCTL_SET_USB_CONFIG,
+				   config, value, 0, NULL);
+}
+EXPORT_SYMBOL_GPL(zynqmp_pm_set_usb_config);
+
 /**
  * struct zynqmp_pm_shutdown_scope - Struct for shutdown scope
  * @subtype:	Shutdown subtype
diff --git a/include/linux/firmware/xlnx-zynqmp.h b/include/linux/firmware/xlnx-zynqmp.h
index c59f997f7633..b03a8994a5be 100644
--- a/include/linux/firmware/xlnx-zynqmp.h
+++ b/include/linux/firmware/xlnx-zynqmp.h
@@ -218,6 +218,7 @@ enum pm_ioctl_id {
 	/* Dynamic SD/GEM configuration */
 	IOCTL_SET_SD_CONFIG = 30,
 	IOCTL_SET_GEM_CONFIG = 31,
+	IOCTL_SET_USB_CONFIG = 32,
 };
 
 enum pm_query_id {
@@ -529,6 +530,10 @@ enum pm_feature_config_id {
 	PM_FEATURE_EXTWDT_VALUE = 4,
 };
 
+enum pm_usb_config_type {
+	USB_CONFIG_FIXED = 1, /* To set fixed config registers */
+};
+
 /**
  * enum pm_sd_config_type - PM SD configuration.
  * @SD_CONFIG_EMMC_SEL: To set SD_EMMC_SEL in CTRL_REG_SD and SD_SLOTTYPE
@@ -677,6 +682,8 @@ int zynqmp_pm_register_sgi(u32 sgi_num, u32 reset);
 int zynqmp_pm_set_sd_config(u32 node, enum pm_sd_config_type config, u32 value);
 int zynqmp_pm_set_gem_config(u32 node, enum pm_gem_config_type config,
 			     u32 value);
+int zynqmp_pm_set_usb_config(u32 node, enum pm_usb_config_type config,
+			     u32 value);
 #else
 static inline int zynqmp_pm_get_api_version(u32 *version)
 {
@@ -1151,6 +1158,13 @@ static inline int zynqmp_pm_set_gem_config(u32 node,
 	return -ENODEV;
 }
 
+static inline int zynqmp_pm_set_usb_config(u32 node,
+					   enum pm_usb_config_type config,
+					   u32 value)
+{
+	return -ENODEV;
+}
+
 #endif
 
 #endif /* __FIRMWARE_ZYNQMP_H__ */
-- 
2.34.1

