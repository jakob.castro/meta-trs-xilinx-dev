Upstream-Status: Pending
From e46d11824e87145721ff7d69064e7686665c4a2b Mon Sep 17 00:00:00 2001
From: Satish Kumar Nagireddy <satish.nagireddy.nagireddy@xilinx.com>
Date: Tue, 5 Mar 2019 17:54:14 -0800
Subject: [PATCH 161/295] v4l: xilinx: dma: Add s_ctrl ioctl for low latency
 capture

This patch adds s_ctrl ioctl to support low latency capture. This will
enable a boolean flag based on control id and pass the same to DMA
controller.

Signed-off-by: Satish Kumar Nagireddy <satish.nagireddy.nagireddy@xilinx.com>
Reviewed-by: Hyun Kwon <hyun.kwon@xilinx.com>
State: pending
[michals: Squashed with
v4l: xilinx: dma: add input ioctls
v4l: xilinx: dma: add v4l2_ctrl_handler
v4l: xilinx: dma: Copy format resolution to crop rectangle
v4l: xilinx: dma: Check subdev resolution against crop rectangle
v4l: xilinx: dma: Add S_SELECTION and G_SELECTION ioctls
v4l: xilinx: dma: Use crop rectangle to prepare dma descriptor
v4l: xilinx: dma: Don't set low latency capture mode on dma
v4l: xilinx: dma: Don't start DMA in STREAM_ON for low latency capture
v4l: xilinx: dma: Perform early callback on queued buffers
v4l: xilinx: dma: Add support to start DMA using s_ctrl
v4l: xilinx: dma: Use early callback mode for low latency capture
v4l: xilinx: dma: Early dequeue first buffer for low latency capture
v4l: xilinx: dma: Disable auto restart for low latency capture
v4l: xilinx: dma: Attach s_ctrl to ctrl_handler instead of ioctl
]
---
 drivers/media/platform/xilinx/xilinx-dma.c | 326 +++++++++++++++++++--
 drivers/media/platform/xilinx/xilinx-dma.h |   8 +
 include/uapi/linux/xilinx-v4l2-controls.h  |   7 +
 3 files changed, 318 insertions(+), 23 deletions(-)

diff --git a/drivers/media/platform/xilinx/xilinx-dma.c b/drivers/media/platform/xilinx/xilinx-dma.c
index e07eeb716261..7be05edc4c20 100644
--- a/drivers/media/platform/xilinx/xilinx-dma.c
+++ b/drivers/media/platform/xilinx/xilinx-dma.c
@@ -16,6 +16,7 @@
 #include <linux/module.h>
 #include <linux/of.h>
 #include <linux/slab.h>
+#include <linux/xilinx-v4l2-controls.h>
 
 #include <media/v4l2-dev.h>
 #include <media/v4l2-fh.h>
@@ -66,7 +67,6 @@ static int xvip_dma_verify_format(struct xvip_dma *dma)
 	struct v4l2_subdev_format fmt;
 	struct v4l2_subdev *subdev;
 	int ret;
-	int width, height;
 
 	subdev = xvip_dma_remote_subdev(&dma->pad, &fmt.pad);
 	if (!subdev)
@@ -80,15 +80,12 @@ static int xvip_dma_verify_format(struct xvip_dma *dma)
 	if (dma->fmtinfo->code != fmt.format.code)
 		return -EINVAL;
 
-	if (V4L2_TYPE_IS_MULTIPLANAR(dma->format.type)) {
-		width = dma->format.fmt.pix_mp.width;
-		height = dma->format.fmt.pix_mp.height;
-	} else {
-		width = dma->format.fmt.pix.width;
-		height = dma->format.fmt.pix.height;
-	}
-
-	if (width != fmt.format.width || height != fmt.format.height)
+	/*
+	 * Crop rectangle contains format resolution by default, and crop
+	 * rectangle if s_selection is executed.
+	 */
+	if (dma->r.width != fmt.format.width ||
+	    dma->r.height != fmt.format.height)
 		return -EINVAL;
 
 	return 0;
@@ -529,6 +526,7 @@ static void xvip_dma_buffer_queue(struct vb2_buffer *vb)
 	u32 luma_size;
 	u32 padding_factor_nume, padding_factor_deno, bpl_nume, bpl_deno;
 	u32 fid = ~0;
+	u32 bpl;
 
 	if (dma->queue.type == V4L2_BUF_TYPE_VIDEO_CAPTURE ||
 	    dma->queue.type == V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE) {
@@ -554,6 +552,8 @@ static void xvip_dma_buffer_queue(struct vb2_buffer *vb)
 		struct v4l2_pix_format_mplane *pix_mp;
 
 		pix_mp = &dma->format.fmt.pix_mp;
+		bpl = pix_mp->plane_fmt[0].bytesperline;
+
 		xilinx_xdma_v4l2_config(dma->dma, pix_mp->pixelformat);
 		xvip_width_padding_factor(pix_mp->pixelformat,
 					  &padding_factor_nume,
@@ -561,12 +561,11 @@ static void xvip_dma_buffer_queue(struct vb2_buffer *vb)
 		xvip_bpl_scaling_factor(pix_mp->pixelformat, &bpl_nume,
 					&bpl_deno);
 		dma->xt.frame_size = dma->fmtinfo->num_planes;
-		dma->sgl[0].size = (pix_mp->width * dma->fmtinfo->bpl_factor *
+		dma->sgl[0].size = (dma->r.width * dma->fmtinfo->bpl_factor *
 				    padding_factor_nume * bpl_nume) /
 				    (padding_factor_deno * bpl_deno);
-		dma->sgl[0].icg = pix_mp->plane_fmt[0].bytesperline -
-							dma->sgl[0].size;
-		dma->xt.numf = pix_mp->height;
+		dma->sgl[0].icg = bpl - dma->sgl[0].size;
+		dma->xt.numf = dma->r.height;
 
 		/*
 		 * dst_icg is the number of bytes to jump after last luma addr
@@ -575,14 +574,14 @@ static void xvip_dma_buffer_queue(struct vb2_buffer *vb)
 
 		/* Handling contiguous data with mplanes */
 		if (dma->fmtinfo->buffers == 1) {
-			dma->sgl[0].dst_icg = 0;
+			dma->sgl[0].dst_icg = bpl *
+					      (pix_mp->height - dma->r.height);
 		} else {
 			/* Handling non-contiguous data with mplanes */
 			if (dma->fmtinfo->buffers == 2) {
 				dma_addr_t chroma_addr =
 					vb2_dma_contig_plane_dma_addr(vb, 1);
-				luma_size = pix_mp->plane_fmt[0].bytesperline *
-					    dma->xt.numf;
+				luma_size = bpl * dma->xt.numf;
 				if (chroma_addr > addr)
 					dma->sgl[0].dst_icg = chroma_addr -
 							      addr - luma_size;
@@ -592,6 +591,7 @@ static void xvip_dma_buffer_queue(struct vb2_buffer *vb)
 		struct v4l2_pix_format *pix;
 
 		pix = &dma->format.fmt.pix;
+		bpl = pix->bytesperline;
 		xilinx_xdma_v4l2_config(dma->dma, pix->pixelformat);
 		xvip_width_padding_factor(pix->pixelformat,
 					  &padding_factor_nume,
@@ -599,12 +599,13 @@ static void xvip_dma_buffer_queue(struct vb2_buffer *vb)
 		xvip_bpl_scaling_factor(pix->pixelformat, &bpl_nume,
 					&bpl_deno);
 		dma->xt.frame_size = dma->fmtinfo->num_planes;
-		dma->sgl[0].size = (pix->width * dma->fmtinfo->bpl_factor *
+		dma->sgl[0].size = (dma->r.width * dma->fmtinfo->bpl_factor *
 				    padding_factor_nume * bpl_nume) /
 				    (padding_factor_deno * bpl_deno);
-		dma->sgl[0].icg = pix->bytesperline - dma->sgl[0].size;
-		dma->xt.numf = pix->height;
+		dma->sgl[0].icg = bpl - dma->sgl[0].size;
+		dma->xt.numf = dma->r.height;
 		dma->sgl[0].dst_icg = 0;
+		dma->sgl[0].dst_icg = bpl * (pix->height - dma->r.height);
 	}
 
 	desc = dmaengine_prep_interleaved_dma(dma->dma, &dma->xt, flags);
@@ -630,6 +631,13 @@ static void xvip_dma_buffer_queue(struct vb2_buffer *vb)
 	list_add_tail(&buf->queue, &dma->queued_bufs);
 	spin_unlock_irq(&dma->queued_lock);
 
+	/*
+	 * Low latency capture: Give descriptor callback at start of
+	 * processing the descriptor
+	 */
+	if (dma->low_latency_cap)
+		xilinx_xdma_set_earlycb(dma->dma, desc,
+					EARLY_CALLBACK_START_DESC);
 	dmaengine_submit(desc);
 
 	if (vb2_is_streaming(&dma->queue))
@@ -674,8 +682,20 @@ static int xvip_dma_start_streaming(struct vb2_queue *vq, unsigned int count)
 
 	/* Start the DMA engine. This must be done before starting the blocks
 	 * in the pipeline to avoid DMA synchronization issues.
+	 * We dont't want to start DMA in case of low latency capture mode,
+	 * applications will start DMA using S_CTRL at later point of time.
 	 */
-	dma_async_issue_pending(dma->dma);
+	if (!dma->low_latency_cap) {
+		dma_async_issue_pending(dma->dma);
+	} else {
+		/* For low latency capture, return the first buffer early
+		 * so that consumer can initialize until we start DMA.
+		 */
+		buf = list_first_entry(&dma->queued_bufs,
+				       struct xvip_dma_buffer, queue);
+		xvip_dma_complete(buf);
+		buf->desc->callback = NULL;
+	}
 
 	/* Start the pipeline. */
 	ret = xvip_pipeline_set_stream(pipe, true);
@@ -809,6 +829,47 @@ static int xvip_xdma_enum_fmt(struct xvip_dma *dma, struct v4l2_fmtdesc *f,
 	return 0;
 }
 
+static int
+xvip_dma_enum_input(struct file *file, void *priv, struct v4l2_input *i)
+{
+	struct v4l2_fh *vfh = file->private_data;
+	struct xvip_dma *dma = to_xvip_dma(vfh->vdev);
+	struct v4l2_subdev *subdev;
+
+	if (i->index > 0)
+		return -EINVAL;
+
+	subdev = xvip_dma_remote_subdev(&dma->pad, NULL);
+	if (!subdev)
+		return -EPIPE;
+
+	/*
+	 * FIXME: right now only camera input type is handled.
+	 * There should be mechanism to distinguish other types of
+	 * input like V4L2_INPUT_TYPE_TUNER and V4L2_INPUT_TYPE_TOUCH.
+	 */
+	i->type = V4L2_INPUT_TYPE_CAMERA;
+	strlcpy(i->name, subdev->name, sizeof(i->name));
+
+	return 0;
+}
+
+static int
+xvip_dma_get_input(struct file *file, void *fh, unsigned int *i)
+{
+	*i = 0;
+	return 0;
+}
+
+static int
+xvip_dma_set_input(struct file *file, void *fh, unsigned int i)
+{
+	if (i > 0)
+		return -EINVAL;
+
+	return 0;
+}
+
 /* FIXME: without this callback function, some applications are not configured
  * with correct formats, and it results in frames in wrong format. Whether this
  * callback needs to be required is not clearly defined, so it should be
@@ -1052,16 +1113,126 @@ xvip_dma_set_format(struct file *file, void *fh, struct v4l2_format *format)
 	if (vb2_is_busy(&dma->queue))
 		return -EBUSY;
 
-	if (V4L2_TYPE_IS_MULTIPLANAR(dma->format.type))
+	if (V4L2_TYPE_IS_MULTIPLANAR(dma->format.type)) {
 		dma->format.fmt.pix_mp = format->fmt.pix_mp;
-	else
+
+		/*
+		 * Save format resolution in crop rectangle. This will be
+		 * updated when s_slection is called.
+		 */
+		dma->r.width = format->fmt.pix_mp.width;
+		dma->r.height = format->fmt.pix_mp.height;
+	} else {
 		dma->format.fmt.pix = format->fmt.pix;
+		dma->r.width = format->fmt.pix.width;
+		dma->r.height = format->fmt.pix.height;
+	}
 
 	dma->fmtinfo = info;
 
 	return 0;
 }
 
+static int
+xvip_dma_g_selection(struct file *file, void *fh, struct v4l2_selection *sel)
+{
+	struct v4l2_fh *vfh = file->private_data;
+	struct xvip_dma *dma = to_xvip_dma(vfh->vdev);
+	u32 width, height;
+	bool crop_frame = false;
+
+	switch (sel->target) {
+	case V4L2_SEL_TGT_COMPOSE:
+		if (sel->type != V4L2_BUF_TYPE_VIDEO_CAPTURE)
+			return -EINVAL;
+
+		crop_frame = true;
+		break;
+	case V4L2_SEL_TGT_COMPOSE_BOUNDS:
+	case V4L2_SEL_TGT_COMPOSE_DEFAULT:
+		if (sel->type != V4L2_BUF_TYPE_VIDEO_CAPTURE)
+			return -EINVAL;
+		break;
+	case V4L2_SEL_TGT_CROP:
+		if (sel->type != V4L2_BUF_TYPE_VIDEO_OUTPUT)
+			return -EINVAL;
+
+		crop_frame = true;
+		break;
+	case V4L2_SEL_TGT_CROP_BOUNDS:
+	case V4L2_SEL_TGT_CROP_DEFAULT:
+		if (sel->type != V4L2_BUF_TYPE_VIDEO_OUTPUT)
+			return -EINVAL;
+		break;
+	default:
+		return -EINVAL;
+	}
+
+	sel->r.left = 0;
+	sel->r.top = 0;
+
+	if (crop_frame) {
+		sel->r.width = dma->r.width;
+		sel->r.height = dma->r.height;
+	} else {
+		if (V4L2_TYPE_IS_MULTIPLANAR(dma->format.type)) {
+			width = dma->format.fmt.pix_mp.width;
+			height = dma->format.fmt.pix_mp.height;
+		} else {
+			width = dma->format.fmt.pix.width;
+			height = dma->format.fmt.pix.height;
+		}
+
+		sel->r.width = width;
+		sel->r.height = height;
+	}
+
+	return 0;
+}
+
+static int
+xvip_dma_s_selection(struct file *file, void *fh, struct v4l2_selection *sel)
+{
+	struct v4l2_fh *vfh = file->private_data;
+	struct xvip_dma *dma = to_xvip_dma(vfh->vdev);
+	u32 width, height;
+
+	switch (sel->target) {
+	case V4L2_SEL_TGT_COMPOSE:
+		/* COMPOSE target is only valid for capture buftype */
+		if (sel->type != V4L2_BUF_TYPE_VIDEO_CAPTURE)
+			return -EINVAL;
+		break;
+	case V4L2_SEL_TGT_CROP:
+		/* CROP target is only valid for output buftype */
+		if (sel->type != V4L2_BUF_TYPE_VIDEO_OUTPUT)
+			return -EINVAL;
+		break;
+	default:
+		return -EINVAL;
+	}
+
+	if (V4L2_TYPE_IS_MULTIPLANAR(dma->format.type)) {
+		width = dma->format.fmt.pix_mp.width;
+		height = dma->format.fmt.pix_mp.height;
+	} else {
+		width = dma->format.fmt.pix.width;
+		height = dma->format.fmt.pix.height;
+	}
+
+	if (sel->r.width > width || sel->r.height > height ||
+	    sel->r.top != 0 || sel->r.left != 0)
+		return -EINVAL;
+
+	sel->r.width = roundup(max(XVIP_DMA_MIN_WIDTH, sel->r.width),
+			       dma->align);
+	sel->r.height = max(XVIP_DMA_MIN_HEIGHT, sel->r.height);
+	dma->r.width = sel->r.width;
+	dma->r.height = sel->r.height;
+
+	return 0;
+}
+
 static const struct v4l2_ioctl_ops xvip_dma_ioctl_ops = {
 	.vidioc_querycap		= xvip_dma_querycap,
 	.vidioc_enum_fmt_vid_cap	= xvip_dma_enum_format,
@@ -1078,6 +1249,8 @@ static const struct v4l2_ioctl_ops xvip_dma_ioctl_ops = {
 	.vidioc_try_fmt_vid_cap_mplane	= xvip_dma_try_format,
 	.vidioc_try_fmt_vid_out		= xvip_dma_try_format,
 	.vidioc_try_fmt_vid_out_mplane	= xvip_dma_try_format,
+	.vidioc_s_selection		= xvip_dma_s_selection,
+	.vidioc_g_selection		= xvip_dma_g_selection,
 	.vidioc_reqbufs			= vb2_ioctl_reqbufs,
 	.vidioc_querybuf		= vb2_ioctl_querybuf,
 	.vidioc_qbuf			= vb2_ioctl_qbuf,
@@ -1086,6 +1259,78 @@ static const struct v4l2_ioctl_ops xvip_dma_ioctl_ops = {
 	.vidioc_expbuf			= vb2_ioctl_expbuf,
 	.vidioc_streamon		= vb2_ioctl_streamon,
 	.vidioc_streamoff		= vb2_ioctl_streamoff,
+	.vidioc_enum_input	= &xvip_dma_enum_input,
+	.vidioc_g_input		= &xvip_dma_get_input,
+	.vidioc_s_input		= &xvip_dma_set_input,
+};
+
+/* -----------------------------------------------------------------------------
+ * V4L2 controls
+ */
+
+static int xvip_dma_s_ctrl(struct v4l2_ctrl *ctl)
+{
+	struct xvip_dma *dma = container_of(ctl->handler, struct xvip_dma,
+					    ctrl_handler);
+	int ret = 0;
+
+	switch (ctl->id)  {
+	case V4L2_CID_XILINX_LOW_LATENCY:
+		if (ctl->val == XVIP_LOW_LATENCY_ENABLE) {
+			if (vb2_is_busy(&dma->queue))
+				return -EBUSY;
+
+			dma->low_latency_cap = true;
+			/*
+			 * Don't use auto-restart for low latency
+			 * to avoid extra one frame delay between
+			 * programming and actual writing of data
+			 */
+			xilinx_xdma_set_mode(dma->dma, DEFAULT);
+		} else if (ctl->val == XVIP_LOW_LATENCY_DISABLE) {
+			if (vb2_is_busy(&dma->queue))
+				return -EBUSY;
+
+			dma->low_latency_cap = false;
+			xilinx_xdma_set_mode(dma->dma, AUTO_RESTART);
+		} else if (ctl->val == XVIP_START_DMA) {
+			/*
+			 * In low latency capture, the driver allows application
+			 * to start dma when queue has buffers. That's why we
+			 * don't check for vb2_is_busy().
+			 */
+			if (dma->low_latency_cap &&
+			    vb2_is_streaming(&dma->queue))
+				dma_async_issue_pending(dma->dma);
+			else
+				ret = -EINVAL;
+		} else {
+			ret = -EINVAL;
+		}
+
+		break;
+	default:
+		ret = -EINVAL;
+	}
+
+	return ret;
+}
+
+static const struct v4l2_ctrl_ops xvip_dma_ctrl_ops = {
+	.s_ctrl = xvip_dma_s_ctrl,
+};
+
+static const struct v4l2_ctrl_config xvip_dma_ctrls[] = {
+	{
+		.ops = &xvip_dma_ctrl_ops,
+		.id = V4L2_CID_XILINX_LOW_LATENCY,
+		.name = "Low Latency Controls",
+		.type = V4L2_CTRL_TYPE_INTEGER,
+		.min = XVIP_LOW_LATENCY_ENABLE,
+		.max = XVIP_START_DMA,
+		.step = 1,
+		.def = XVIP_LOW_LATENCY_DISABLE,
+	}
 };
 
 /* -----------------------------------------------------------------------------
@@ -1175,6 +1420,40 @@ int xvip_dma_init(struct xvip_composite_device *xdev, struct xvip_dma *dma,
 	if (ret < 0)
 		goto error;
 
+	ret = v4l2_ctrl_handler_init(&dma->ctrl_handler,
+				     ARRAY_SIZE(xvip_dma_ctrls));
+	if (ret < 0) {
+		dev_err(dma->xdev->dev, "failed to initialize V4L2 ctrl\n");
+		goto error;
+	}
+
+	for (i = 0; i < ARRAY_SIZE(xvip_dma_ctrls); i++) {
+		struct v4l2_ctrl *ctrl;
+
+		dev_dbg(dma->xdev->dev, "%d ctrl = 0x%x\n", i,
+			xvip_dma_ctrls[i].id);
+		ctrl = v4l2_ctrl_new_custom(&dma->ctrl_handler,
+					    &xvip_dma_ctrls[i], NULL);
+		if (!ctrl) {
+			dev_err(dma->xdev->dev, "Failed for %s ctrl\n",
+				xvip_dma_ctrls[i].name);
+			goto error;
+		}
+	}
+
+	if (dma->ctrl_handler.error) {
+		dev_err(dma->xdev->dev, "failed to add controls\n");
+		ret = dma->ctrl_handler.error;
+		goto error;
+	}
+
+	dma->video.ctrl_handler = &dma->ctrl_handler;
+	ret = v4l2_ctrl_handler_setup(&dma->ctrl_handler);
+	if (ret < 0) {
+		dev_err(dma->xdev->dev, "failed to set controls\n");
+		goto error;
+	}
+
 	/* ... and the video node... */
 	dma->video.fops = &xvip_dma_fops;
 	dma->video.v4l2_dev = &xdev->v4l2_dev;
@@ -1271,6 +1550,7 @@ void xvip_dma_cleanup(struct xvip_dma *dma)
 	if (!IS_ERR_OR_NULL(dma->dma))
 		dma_release_channel(dma->dma);
 
+	v4l2_ctrl_handler_free(&dma->ctrl_handler);
 	media_entity_cleanup(&dma->video.entity);
 
 	mutex_destroy(&dma->lock);
diff --git a/drivers/media/platform/xilinx/xilinx-dma.h b/drivers/media/platform/xilinx/xilinx-dma.h
index cd81df6dbb2d..a8d39b504704 100644
--- a/drivers/media/platform/xilinx/xilinx-dma.h
+++ b/drivers/media/platform/xilinx/xilinx-dma.h
@@ -18,6 +18,7 @@
 #include <linux/videodev2.h>
 
 #include <media/media-entity.h>
+#include <media/v4l2-ctrls.h>
 #include <media/v4l2-dev.h>
 #include <media/videobuf2-v4l2.h>
 
@@ -61,11 +62,13 @@ static inline struct xvip_pipeline *to_xvip_pipeline(struct video_device *vdev)
  * @video: V4L2 video device associated with the DMA channel
  * @pad: media pad for the video device entity
  * @remote_subdev_med_bus: media bus format of sub-device
+ * @ctrl_handler: V4L2 ctrl_handler for inheritance ctrls from subdev
  * @xdev: composite device the DMA channel belongs to
  * @pipe: pipeline belonging to the DMA channel
  * @port: composite device DT node port number for the DMA channel
  * @lock: protects the @format, @fmtinfo and @queue fields
  * @format: active V4L2 pixel format
+ * @r: crop rectangle parameters
  * @fmtinfo: format information corresponding to the active @format
  * @poss_v4l2_fmts: All possible v4l formats supported
  * @poss_v4l2_fmt_cnt: number of supported v4l formats
@@ -78,6 +81,7 @@ static inline struct xvip_pipeline *to_xvip_pipeline(struct video_device *vdev)
  * @xt: dma interleaved template for dma configuration
  * @sgl: data chunk structure for dma_interleaved_template
  * @prev_fid: Previous Field ID
+ * @low_latency_cap: Low latency capture mode
  */
 struct xvip_dma {
 	struct list_head list;
@@ -85,12 +89,15 @@ struct xvip_dma {
 	struct media_pad pad;
 	u32 remote_subdev_med_bus;
 
+	struct v4l2_ctrl_handler ctrl_handler;
+
 	struct xvip_composite_device *xdev;
 	struct xvip_pipeline pipe;
 	unsigned int port;
 
 	struct mutex lock;
 	struct v4l2_format format;
+	struct v4l2_rect r;
 	const struct xvip_video_format *fmtinfo;
 	u32 *poss_v4l2_fmts;
 	u32 poss_v4l2_fmt_cnt;
@@ -107,6 +114,7 @@ struct xvip_dma {
 	struct data_chunk sgl[1];
 
 	u32 prev_fid;
+	u32 low_latency_cap;
 };
 
 #define to_xvip_dma(vdev)	container_of(vdev, struct xvip_dma, video)
diff --git a/include/uapi/linux/xilinx-v4l2-controls.h b/include/uapi/linux/xilinx-v4l2-controls.h
index 6a25febfb2d7..23d1574c6d55 100644
--- a/include/uapi/linux/xilinx-v4l2-controls.h
+++ b/include/uapi/linux/xilinx-v4l2-controls.h
@@ -213,6 +213,13 @@
 /* Low latency mode */
 #define V4L2_CID_XILINX_LOW_LATENCY		(V4L2_CID_XILINX_VIP + 1)
 
+/* Control values to enable/disable low latency capture mode */
+#define XVIP_LOW_LATENCY_ENABLE		BIT(1)
+#define XVIP_LOW_LATENCY_DISABLE	BIT(2)
+
+/* Control value to start DMA */
+#define XVIP_START_DMA			BIT(3)
+
 /*
  * Xilinx SCD
  */
-- 
2.34.1

